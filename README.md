Résumé of Amy Dee Dempster
===========================

This Git repository contains this short résumé (i.e. summary) written in Markdown,
and a longer curriculum vitæ written in XeLaTeX and exported to PDF for download.

## Career Profile

As a communications expert, I have spent my adult life absorbing a wide
range of knowledge and working in several language fields
simultaneously, before moving into Information Technology.

## Skills

### Platforms

After the Commodore computers of my youth, I gained extensive experience using
different modern platforms: Windows since 1997, GNU/Linux (.deb and .rpm based)
since 2003, Mac OS X since 2013, with an ability to perform most administrative roles
across these systems.

### Design

I am proficient in image manipulation with tools such as GIMP and Inkscape and can
hand-code vector graphics in SVG. I am very enthusiastic about usability, aesthetics and
UX principles, as I believe that a polished IT product should be a pleasure to use.

### Markup & programming languages

Very familiar with
XeLaTeX,
HTML 5,
CSS 3,
JavaScript/Node.js/JQuery,
Python 3 
and Perl 5.
Have also done university courses in Java & SQL,
and SoloLearn courses in PHP and Ruby.

### Natural language

Native English-speaker, also taken for a native speaker of French, Spanish and Italian.
Accredited as an interpreter and translator in these languages. Accredited as a teacher
in English and Spanish as foreign languages. Conversant in Catalan and Portuguese.
Upper-elementary German (four years at school), Norwegian, Swedish, Danish, Dutch,
and Mandarin (knowledge of ~400 Chinese characters). One semester of Japanese at
Macquarie University. Experienced tutor of Latin.


## Projects / Portfolio

See here (on GitHub/Bitbucket) under the username ‘AmyDeeDempster’.

## Work Experience

* Freelance language specialist (tutor, translator, interpreter) since 1999, now phasing out this work in favour of I.T.
* Occasional freelance web design work.

See full CV for details.

## Degrees

See full CV for further details of qualifications.

### Bachelor of Information Technology, 2015–present, RMIT University

Includes a minor in natural science, and several free electives.
So far completed 16 units out of 24.

*Core units completed:*
Introduction to I.T.,
Introduction to Programming,
Programming 1,
User-Centred Design,
Database Concepts,
Building IT Systems,
Web Programming,
Introduction to Computer Systems,
Programming in C,
Scripting Programming Languages.

### B.A. Modern Languages (French/Spanish), 1997–2001, University of Bradford

A four-year degree focused on translation,
interpreting and the culture of the countries in question,
with a placement year split between France and Spain.
Second-class honours, first division.
